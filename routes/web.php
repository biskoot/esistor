<?php

use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CrudController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\MeasurementUnitController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductDataController;
use App\Http\Controllers\ProductLedgerController;
use App\Http\Controllers\ProductPurchaseController;
use App\Http\Controllers\StorageController;
use App\Http\Controllers\StorageTypeController;
use App\Http\Controllers\SubmissionController;
use App\Http\Controllers\SubmissionStatusController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\TemplateController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('dashboard', DashboardController::class)->name('dashboard');
Route::resource('crud', CrudController::class);

Route::resource('item', ItemController::class);
Route::resource('brand', BrandController::class);
Route::resource('storage', StorageController::class);
Route::resource('storage-type', StorageTypeController::class);
Route::resource('category', CategoryController::class);
Route::resource('supplier', SupplierController::class);
Route::resource('submission-status', SubmissionStatusController::class);
Route::resource('measurement-unit', MeasurementUnitController::class);
Route::resource('product', ProductController::class);
Route::resource('product-purchase', ProductPurchaseController::class);
Route::resource('product-ledger', ProductLedgerController::class);
Route::resource('product-data', ProductDataController::class);
Route::resource('submission', SubmissionController::class);

// Template route
Route::group(['prefix' => 'template', 'as' => 'template.'], function () {
    Route::get('{file}.html', TemplateController::class)->name('dashboard');
});
