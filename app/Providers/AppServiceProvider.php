<?php

namespace App\Providers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Blueprint::macro('default', function ($enable_status_code = true, $precision = 0) {
            if ($enable_status_code) {
                $this->integer('status_code')->default(1)->comment('1-aktif,0-arkib,kod_status');
            }
            $this->timestamp('created_at', $precision)->nullable()->comment('tkh_masuk');
            $this->string('created_by', 12)->nullable()->comment('masuk_oleh');
            $this->timestamp('updated_at', $precision)->nullable()->comment('tkh_kemaskini');
            $this->string('updated_by', 12)->nullable()->comment('kemaskini_oleh');
        });
    }
}
