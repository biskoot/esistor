<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'status_code' => 'required',
            'created_at' => 'nullable',
            'created_by' => 'nullable',
            'updated_at' => 'update_at',
            'updated_by' => 'updated_by',        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'masukkan jenama brand',
            'status_code.required' => 'pilih status code',
        ];
    }
}
