<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductLedgerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'product_id' => 'required',
            'product_purchase_id' => 'required',
            'is_in_out' => 'required',
            'unit_price' => 'required',
            'qty_in' => 'required',
            'qty_out' => 'required',
            'total_price' => 'required',
            'balance' => 'required',
            'current_value' => 'required',
        ];
    }
}
