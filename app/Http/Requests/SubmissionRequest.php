<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubmissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'product_id' => 'required',
            'submission_status_id' => 'required',
            'qty' => 'required',
            'requested_by' => 'required',
            'validated_at' => 'nullable',
            'validated_by' => 'nullable',
            'checked_at' => 'nullable',
            'checked_by' => 'nullable',
            'approved_at' => 'nullable',
            'approved_by' => 'nullable',
            'status_code' => 'required',
        ];
    }
}
