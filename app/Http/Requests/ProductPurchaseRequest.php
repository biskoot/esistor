<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductPurchaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'product_id' => 'required',
            'supplier_id' => 'required',
            'lo_no' => 'required',
            'unit_price' => ['required', 'numeric'],
            'qty' => ['required', 'numeric'],
            'total_price' => ['required', 'numeric'],
            'balance' => ['required', 'numeric'],
        ];
    }

    public function messages()
    {
        return [
            'product_id.required' => 'Sila pilih/masukkan data.',
            'supplier_id.required' => 'Sila pilih/masukkan data.',
            'lo_no.required' => 'Sila pilih/masukkan data.',
            'unit_price.required' => 'Sila pilih/masukkan data.',
            'qty.required' => 'Sila pilih/masukkan data.',
        ];
    }
}
