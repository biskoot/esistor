<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductPurchaseRequest;
use App\Models\ProductPurchase;
use DB;

class ProductPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $models = ProductPurchase::all();
        $data['models'] = $models;

        // dd($data['models']);
        return view('product-purchase.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('product-purchase.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductPurchaseRequest $request)
    {
        $data = $request->validated();
        try {
            DB::beginTransaction();

            $data['total_price'] = $data['qty'] * $data['unit_price'];
            ProductPurchase::create($data);

            DB::commit();
            session()->flash('success', 'Item has been added.');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(ProductPurchase $productPurchase)
    {
        $data['product_purchase'] = ProductPurchase::find($productPurchase->id);

        return view('crud.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ProductPurchase $productPurchase)
    {
        $data['product_purchase'] = ProductPurchase::find($productPurchase->id);

        return view('crud.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductPurchaseRequest $request, ProductPurchase $productPurchase)
    {
        $data = $request->validated();
        try {
            DB::beginTransaction();
            $productPurchase->update($data);

            DB::commit();
            session()->flash('success', 'Item has been updated.');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ProductPurchase $productPurchase)
    {
        // $data['product_purchase'] = ProductPurchase::find($productPurchase->id);
        try {
            DB::beginTransaction();
            $productPurchase->delete();
            DB::commit();
            session()->flash('success', 'Item has been deleted.');

            return redirect()->route('product-purchase.index');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }
}
