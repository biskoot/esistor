<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductLedgerRequest;
use App\Models\ProductLedger;

class ProductLedgerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductLedgerRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ProductLedger $productLedger)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ProductLedger $productLedger)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductLedgerRequest $request, ProductLedger $productLedger)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ProductLedger $productLedger)
    {
        //
    }
}
