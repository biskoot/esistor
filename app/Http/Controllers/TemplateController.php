<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TemplateController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request, string $filename)
    {
        $template = 'velonicadmin';

        return view('template.'.$template.'.'.$filename);
    }
}
