<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\MeasurementUnit;
use App\Models\Product;
use App\Models\ProductData;
use App\Models\Storage;
use App\Models\StorageType;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Product::listData()->get();

        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $productDatas = ProductData::list()->get();
        $storageTypes = StorageType::list()->get();
        $storages = Storage::list()->get();
        $measurementUnits = MeasurementUnit::list()->get();

        return view('product.create', compact('productDatas', 'storageTypes', 'storages', 'measurementUnits'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductRequest $request)
    {
        $data = $request->validated();

        try {
            DB::beginTransaction();

            Product::create($data);

            DB::commit();
            session()->flash('success', 'Product has been added.');

            return redirect()->route('product.index');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductRequest $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        //
    }
}
