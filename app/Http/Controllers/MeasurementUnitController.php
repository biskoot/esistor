<?php

namespace App\Http\Controllers;

use App\Http\Requests\MeasurementUnitRequest;
use App\Models\MeasurementUnit;
use Illuminate\Support\Facades\DB;

class MeasurementUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $models = MeasurementUnit::all();
        $data['models'] = $models;

        return view('measurement-unit.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('measurement-unit.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MeasurementUnitRequest $request)
    {
        $data = $request->validated();

        try {
            DB::beginTransaction();

            MeasurementUnit::create($data);

            DB::commit();
            session()->flash('success', 'Item has been added.');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(MeasurementUnit $measurementUnit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(MeasurementUnit $measurementUnit)
    {
        $data['MeasurementUnit'] = $measurementUnit;

        return view('measurement-unit.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MeasurementUnitRequest $request, MeasurementUnit $measurementUnit)
    {

        $data = $request->validated();

        try {
            DB::beginTransaction();

            $measurementUnit->update($data);

            DB::commit();
            session()->flash('success', 'Item has been added.');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(MeasurementUnit $measurementUnit)
    {
        try {
            DB::beginTransaction();

            $measurementUnit->delete();

            DB::commit();
            session()->flash('success', 'Measurement Unit has been deleted.');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }
}
