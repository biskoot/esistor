<?php

namespace App\Http\Controllers;

use App\Http\Requests\CrudRequest;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //

        return view('crud.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //

        return view('crud.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CrudRequest $request)
    {
        //

        session()->flash('success', 'CRUD stored!');

        return redirect()->route('crud.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //

        return view('crud.show');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //

        return view('crud.edit');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CrudRequest $request, string $id)
    {
        //

        session()->flash('success', 'CRUD updated!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //

        return redirect()->route('crud.index');
    }
}
