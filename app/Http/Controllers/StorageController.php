<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorageRequest;
use App\Models\Storage;
use Illuminate\Support\Facades\DB;

class StorageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data['models'] = Storage::all();

        return view('storage.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('storage.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorageRequest $request)
    {
        $data = $request->validated();

        try {
            DB::beginTransaction();

            Storage::create($data);

            DB::commit();
            session()->flash('success', 'Item has been added.');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(Storage $storage)
    {
        return view('storage.show');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Storage $storage)
    {
        $data['edited'] = $storage;

        return view('storage.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StorageRequest $request, Storage $storage)
    {

        $data = $request->validated();

        try {
            DB::beginTransaction();

            $storage->update($data);

            DB::commit();
            session()->flash('success', 'Item has been updated.');

            return redirect()->route('storage.index');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Storage $storage)
    {
        try {
            DB::beginTransaction();

            $storage->delete();

            DB::commit();
            session()->flash('success', 'Item has been deleted.');

            return redirect()->route('storage.index');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }
}
