<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorageTypeRequest;
use App\Models\StorageType;

class StorageTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorageTypeRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(StorageType $storageType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(StorageType $storageType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StorageTypeRequest $request, StorageType $storageType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(StorageType $storageType)
    {
        //
    }
}
