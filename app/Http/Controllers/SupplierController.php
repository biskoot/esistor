<?php

namespace App\Http\Controllers;

use App\Http\Requests\SupplierRequest;
use App\Models\Supplier;
use DB;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $models = Supplier::all();
        $data['models'] = $models;

        return view('supplier.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SupplierRequest $request)
    {
        // session()->flash('success', 'Data stored!');
        // return redirect()->route('supplier.index');

        $data = $request->validated();

        try {
            DB::beginTransaction();

            Supplier::create($data);

            DB::commit();
            session()->flash('success', 'Item has been added.');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Supplier $supplier)
    {
        $data['models'] = Supplier::find($supplier->id);

        return view('supplier.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Supplier $supplier)
    {
        $data['supplier'] = Supplier::find($supplier->id);

        return view('supplier.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(SupplierRequest $request, Supplier $supplier)
    {
        $data = $request->validated();

        try {
            DB::beginTransaction();

            $supplier->update($data);

            DB::commit();
            session()->flash('success', 'Supplier has been added.');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Supplier $supplier)
    {
        try {
            DB::beginTransaction();

            $supplier->delete();

            DB::commit();
            session()->flash('success', 'Supplier has been deleted');

            return redirect()->route('supplier.index');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }
}
