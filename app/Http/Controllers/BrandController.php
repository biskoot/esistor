<?php

namespace App\Http\Controllers;

use App\Http\Requests\BrandRequest;
use App\Models\Brand;
use DB;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $models = Brand::all();
        $data['models'] = $models;

        return view('brand.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('brand.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(BrandRequest $request)
    {

        $data = $request->validated();

        try {
            DB::beginTransaction();

            Brand::create($data);

            DB::commit();
            session()->flash('success', 'Brand has been added.');

            return redirect()->back();
        } catch (\Exception $e) {

            DB::rollback();

            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }

        return view('brand.store');
    }

    /**
     * Display the specified resource.
     */
    public function show(Brand $brand)
    {
        $data['brand'] = $brand;

        return view('brand.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Brand $brand)
    {

        $data['brand'] = $brand;

        return view('brand.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(BrandRequest $request, Brand $brand)
    {
        $data = $request->validated();

        try {
            DB::beginTransaction();

            $brand->update($data);

            DB::commit();
            session()->flash('success', 'Brand has been added.');

            return redirect()->route('brand.index');
        } catch (\Exception $e) {

            DB::rollback();

            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }

        return view('brand.store');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Brand $brand)
    {

        try {
            DB::beginTransaction();

            $brand->delete();

            DB::commit();
            session()->flash('success', 'Brand has been deleted.');

            return redirect()->route('brand.index');
        } catch (\Exception $e) {

            DB::rollback();

            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }
}
