<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubmissionRequest;
use App\Models\Submission;
use Illuminate\Support\Facades\DB;

class SubmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $models = Submission::all();
        $data['models'] = $models;

        return view('submission.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('submission.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SubmissionRequest $request)
    {
        $data = $request->validated();

        try {
            DB::beginTransaction();

            $data['validated_by'] = '950724102025';

            $submission = Submission::create($data);

            DB::commit();
            session()->flash('success', 'Submission has been added.');

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Submission $submission)
    {
        $data['submission'] = $submission;

        return view('submission.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Submission $submission)
    {
        $data['submission'] = $submission;

        return view('submission.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(SubmissionRequest $request, Submission $submission)
    {
        $data = $request->validated();

        try {
            DB::beginTransaction();

            $data['validated_by'] = '112233445566';

            $submission->update($data);

            DB::commit();
            session()->flash('success', 'Submission has been updated.');

            // return redirect()->back();
            return redirect()->route('submission.index');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Submission $submission)
    {
        try {
            DB::beginTransaction();

            $submission->delete();

            DB::commit();
            session()->flash('success', 'Submission has been deleted.');

            return redirect()->route('submission.index');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }
}
