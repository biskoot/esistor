<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductDataRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Item;
use App\Models\ProductData;
use DB;

class ProductDataController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $product_datas = ProductData::get();

        return view('product_data.index', compact('product_datas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::list()->get();
        $brands = Brand::list()->get();
        $items = Item::list()->get();

        return view('product_data.create', compact('categories', 'brands', 'items'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductDataRequest $request)
    {
        $data = $request->validated();

        try {
            DB::beginTransaction();

            ProductData::create($data);

            DB::commit();
            session()->flash('success', 'Product data has been added.');

            return redirect()->route('product-data.index');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('error', $e->getMessage());

            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(ProductData $productData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ProductData $productData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductDataRequest $request, ProductData $productData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ProductData $productData)
    {
        //
    }
}
