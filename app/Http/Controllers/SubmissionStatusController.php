<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubmissionStatusRequest;
use App\Models\SubmissionStatus;

class SubmissionStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SubmissionStatusRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(SubmissionStatus $submissionStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(SubmissionStatus $submissionStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(SubmissionStatusRequest $request, SubmissionStatus $submissionStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(SubmissionStatus $submissionStatus)
    {
        //
    }
}
