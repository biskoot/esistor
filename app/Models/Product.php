<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    protected $fillable = ['product_data_id', 'storage_type_id', 'storage_id', 'measurement_unit_id'];

    public function product_datas()
    {
        return $this->belongsToMany(ProductData::class);
    }

    public function storageType()
    {
        return $this->belongsTo(storageType::class);
    }

    public function storage()
    {
        return $this->belongsTo(Storage::class);
    }

    public function measurement_unit()
    {
        return $this->belongsTo(MeasurementUnit::class);
    }

    public static function listData()
    {
        $data = self::join('product_data', 'product_data.id', 'product_data_id')
            ->join('master_storage_types', 'master_storage_types.id', 'storage_type_id')
            ->join('master_storages', 'master_storages.id', 'storage_id')
            ->join('master_measurement_units', 'master_measurement_units.id', 'measurement_unit_id')
            ->select('products.id', 'product_data_id', 'products.storage_type_id', 'products.storage_id', 'measurement_unit_id', 'master_storage_types.name AS storageTypeName', 'master_storages.name AS storageName', 'master_measurement_units.name AS measureName', 'products.created_at AS created_at');

        return $data;
    }
}
