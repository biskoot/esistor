<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $table = 'master_items';

    protected $fillable = [

        'name',
        'status_code',
        'updated_by',
        'created_by',
    ];

    public static function list()
    {
        $data = self::orderBy('name', 'ASC');

        return $data;
    }
}
