<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductData extends Model
{
    use HasFactory;

    protected $table = 'product_data';

    protected $fillable = ['category_id', 'brand_id', 'item_id', 'about', 'photo', 'status_code'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public static function list()
    {
        $data = self::join('master_items', 'master_items.id', 'item_id')
            ->join('master_brands', 'master_brands.id', 'brand_id')
            ->select('product_data.id', 'master_brands.name AS brand', 'master_items.name AS item');

        return $data;
    }
}
