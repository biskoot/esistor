<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'submission_status_id',
        'qty',
        'requested_by',
        'validated_at',
        'validated_by',
        'checked_at',
        'checked_by',
        'approved_at',
        'approved_by',
        'status_code',
        'created_by',
        'updated_by',
    ];

    public function submission_status()
    {
        return $this->belongsTo(SubmissionStatus::class, 'submission_status_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
