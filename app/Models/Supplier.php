<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'status_code',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];

    protected $table = 'master_suppliers';
}
