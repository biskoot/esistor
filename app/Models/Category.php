<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'master_categories';

    protected $fillable = [
        'name',
        'status_code',
    ];

    public static function list()
    {
        $data = self::orderBy('name', 'ASC');

        return $data;
    }
}
