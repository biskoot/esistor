<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MeasurementUnit extends Model
{
    use HasFactory;

    protected $table = 'master_measurement_units';

    protected $fillable = [
        'name',
        'status_code',
        'created_by',
        'updated_by',
    ];

    public static function list()
    {
        $data = self::orderBy('name', 'ASC');

        return $data;
    }
}
