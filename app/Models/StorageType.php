<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StorageType extends Model
{
    use HasFactory;

    protected $table = 'master_storage_types';

    public static function list()
    {
        $data = self::orderBy('name', 'ASC');

        return $data;
    }
}
