<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Storage extends Model
{
    use HasFactory;

    protected $table = 'master_storages';

    protected $fillable = [
        'storage_type_id',
        'name',
        'status_code',

    ];

    public static function list()
    {
        $data = self::orderBy('name', 'ASC');

        return $data;
    }
}
