<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product_data', function (Blueprint $table) {
            $table->id()->comment('id_item_kategori_jenama');
            $table->foreignId('category_id')->comment('kod_kategori')->constrained('master_categories');
            $table->foreignId('brand_id')->comment('kod_jenama')->constrained('master_brands');
            $table->foreignId('item_id')->comment('kod_item')->constrained('master_items');
            $table->string('about', 200)->comment('perihal');
            $table->string('photo', 100)->nullable()->comment('gambar');
            $table->default();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_data');
    }
};
