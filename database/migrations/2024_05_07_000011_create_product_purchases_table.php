<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product_purchases', function (Blueprint $table) {
            $table->id()->comment('id_purchase_produk');
            $table->foreignId('product_id')->comment('id_produk');
            $table->foreignId('supplier_id')->comment('kod_pembekal');

            // $table->foreignId('product_id')->comment('id_produk')->constrained('products');
            // $table->foreignId('supplier_id')->comment('kod_pembekal')->constrained('master_suppliers');

            $table->string('lo_no', 30)->comment('no_LO');
            $table->decimal('unit_price', 10, 2)->comment('price_per_unit');
            $table->integer('qty')->comment('kuantiti');
            $table->decimal('total_price', 10, 2)->comment('total_harga');
            $table->integer('balance')->comment('baki');
            $table->default(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_purchases');
    }
};
