<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->id()->comment('id_mohon');
            // $table->foreignId('product_id')->comment('id_produk')->constrained('products');
            $table->foreignId('product_id')->comment('id_produk');
            // $table->foreignId('submission_status_id')->comment('kod_status_permohonan')->constrained('master_submission_statuses');
            $table->foreignId('submission_status_id')->comment('kod_status_permohonan');
            $table->integer('qty')->comment('kuantiti');
            $table->string('requested_by', 12)->comment('nokp');
            $table->timestamp('validated_at')->nullable()->comment('tkh_sah');
            $table->string('validated_by', 12)->nullable()->comment('nokp_pengesah');
            $table->timestamp('checked_at')->nullable()->comment('tkh_semak');
            $table->string('checked_by', 12)->nullable()->comment('nokp_penyemak');
            $table->timestamp('approved_at')->nullable()->comment('tkh_lulus');
            $table->string('approved_by', 12)->nullable()->comment('nokp_pelulus');
            $table->default();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('submissions');
    }
};
