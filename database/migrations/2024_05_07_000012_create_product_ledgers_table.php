<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product_ledgers', function (Blueprint $table) {
            $table->id()->comment('id_ledger_stok');
            $table->foreignId('product_id')->comment('id_produk')->constrained('products');
            $table->foreignId('product_purchase_id')->comment('id_purchase_produk')->constrained('product_purchases');
            $table->string('is_in_out', 1)->comment('M-masuk,K-keluar,is_masuk_keluar');
            $table->decimal('unit_price', 10, 2)->comment('price_per_unit');
            $table->integer('qty_in')->comment('kuantiti_terima');
            $table->integer('qty_out')->comment('kuantiti_keluar');
            $table->decimal('total_price', 10, 2)->comment('total_price');
            $table->integer('balance')->comment('baki');
            $table->decimal('current_value', 10, 2)->comment('nila_semasa');
            $table->default(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_ledgers');
    }
};
