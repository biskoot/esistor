<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id()->comment('id_produk');
            $table->foreignId('product_data_id')->comment('id_item_jenama_produk')->constrained('product_data');
            $table->foreignId('storage_type_id')->comment('kod_jenis_stor')->constrained('master_storage_types');
            $table->foreignId('storage_id')->comment('kod_stor')->constrained('master_storages');
            $table->foreignId('measurement_unit_id')->comment('kod_unit_ukuran')->constrained('master_measurement_units');
            $table->default(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
