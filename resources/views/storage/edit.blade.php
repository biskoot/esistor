<x-layout :title="'Edit Storage'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('storage.update', $edited) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Storage Type Id</label>
                                <input type="text" value="{{$edited->storage_type_id}}" id="storage_type_id" name="storage_type_id" class="form-control" placeholder="Storage Type Id">
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Storage Name</label>
                                <input type="text" value="{{$edited->name}}" id="name" name="name" class="form-control" placeholder="Storage Name">
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="Status Code">Status Code</label>
                                <input type="text" value="{{$edited->status_code}}" id="status_code" name="status_code" class="form-control" placeholder="Status Code">
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">
                            <i class="ri-save-line me-1 fs-16 lh-1"></i> Save
                        </button>
                        <a href="{{ route('storage.index') }}" class="btn btn-secondary">
                            <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-layout>
