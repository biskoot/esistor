<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ isset($title) && $title != '' ? $title .' | '. config('app.name') : config('app.name') }}</title>

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('velonicadmin/images/favicon.ico') }}">

    @stack('before-styles')
    <!-- custom css -->
    @stack('after-styles')

    <!-- Theme Config Js -->
    <script src="{{ asset('velonicadmin/js/config.js') }}"></script>
    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('velonicadmin/css/app.min.css') }}" id="app-style" />
    <!-- Icons css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('velonicadmin/css/icons.min.css') }}" />
</head>
<body>
    <!-- Begin page -->
    <div class="wrapper">

        @include('includes.header')

        @include('includes.sidebar')

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">
                <!-- Start Content-->
                <div class="container-fluid">
                    @if($title)
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">{{ config('app.name') }}</a></li>
                                            <li class="breadcrumb-item active">{{ $title }}</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">{{ $title }}</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->
                    @endif

                    @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            <strong>Error!</strong><br>{!! implode("<br>", $errors->all()) !!}
                        </div>
                    @elseif(session()->get('error'))
                        <div class="alert alert-danger" role="alert">
                            <strong>Error!</strong><br>{!! session()->get('error') !!}
                        </div>
                    @elseif(session()->get('success'))
                        <div class="alert alert-primary" role="alert">
                            <strong>Success!</strong><br>{!! session()->get('success') !!}
                        </div>
                    @endif

                    {{ $slot }}
                </div>
                <!-- container -->
            </div>
            <!-- content -->

            @include('includes.footer')
        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->

    </div>
    <!-- END wrapper -->

    @include('includes.theme-settings')


    <!-- Vendor js -->
    <script src="{{ asset('velonicadmin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/simplebar/simplebar.min.js') }}"></script>

    @stack('before-scripts')
    <!-- custom js -->
    @stack('after-scripts')

    <!-- App js -->
    <script src="{{ asset('velonicadmin/js/main.js') }}"></script>
    <script src="{{ asset('velonicadmin/js/layout.js') }}"></script>
</body>
</html>
