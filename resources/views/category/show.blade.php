<x-layout :title="'Show Dummy CRUD'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-condensed mb-0 border-top">
                        <tbody>
                            <tr>
                                <th scope="row">Label Text</th>
                                <td>
                                    Input Value
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Label Text</th>
                                <td>
                                    Input Value
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="{{ route('crud.index') }}" class="btn btn-secondary mt-2">
                        <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                    </a>
                </div>
            </div>
        </div>
    </div>

</x-layout>
