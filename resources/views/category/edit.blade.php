<x-layout :title="'Edit Dummy CRUD'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('category.update', $category) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="Name">Name</label>
                                <input type="text" value="{{$category->name}}" id="name" name="name" class="form-control" placeholder="Input Name">
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="Status Code"> Status Code</label>
                                <input type="number" value="{{$category->status_code}}" id="status__code" name="status_code" class="form-control" placeholder="Input Status Code">
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">
                            <i class="ri-save-line me-1 fs-16 lh-1"></i> Save
                        </button>
                        <a href="{{ route('crud.index') }}" class="btn btn-secondary">
                            <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-layout>
