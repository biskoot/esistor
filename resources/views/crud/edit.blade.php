<x-layout :title="'Edit Product Purchase'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('product-purchase.update',$product_purchase) }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <input type="hidden" name="supplier_id" value='{{$product_purchase->supplier_id}}'>
                        <input type="hidden" name="total_price" value='{{$product_purchase->total_price}}'>
                        <input type="hidden" name="balance" value='44'>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="product_id">Product</label>
                                <select class="form-control" name="product_id" id="product_id">
                                    <option value="">-</option>
                                    <option {{$product_purchase->product_id == 1 ? 'selected' : ''}} value="1">Pensil Picit by Syarikat ABC Sdn Bhd</option>
                                    <option {{$product_purchase->product_id == 2 ? 'selected' : ''}} value="2">Klip Kertas by Syarikat ABC Sdn Bhd</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="lo_no">LO Number</label>
                                <input type="text"  id="lo_no" name="lo_no" class="form-control" value="{{$product_purchase->lo_no}}" placeholder="Enter LO Number" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="unit_price">Price per unit (RM)</label>
                                <input type="number" id="unit_price" step="0.1" name="unit_price" class="form-control" value="{{$product_purchase->unit_price}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="qty">Quantity</label>
                                <input type="number" id="qty" name="qty" class="form-control" value="{{$product_purchase->qty}}">
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">
                            <i class="ri-save-line me-1 fs-16 lh-1"></i> Save
                        </button>
                        <a href="{{ route('product-purchase.index') }}" class="btn btn-secondary">
                            <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-layout>
