<x-layout :title="'Create Product Data'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('product-data.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Kategori</label>
                                <select name="category_id" id="category_id" class="form-control">
                                    <option value="">--Sila Pilih Kategori--</option>
                                    @foreach ($categories as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Jenama</label>
                                <select name="brand_id" id="brand_id" class="form-control">
                                    <option value="">--Sila Pilih Jenama--</option>
                                    @foreach ($brands as $br)
                                        <option value="{{$br->id}}">{{$br->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Item</label>
                                <select name="item_id" id="item_id" class="form-control">
                                    <option value="">--Sila Pilih Jenama--</option>
                                    @foreach ($items as $it)
                                        <option value="{{$it->id}}">{{$it->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Perihal</label>
                               <input type="text" name="about" id="about" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Status</label>
                                <select name="status_code" id="status_code" class="form-control">
                                    <option value="1">AKTIF</option>
                                    <option value="0">ARKIB</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">
                            <i class="ri-add-box-line me-1 fs-16 lh-1"></i> Create
                        </button>
                        <a href="{{ route('product-data.index') }}" class="btn btn-secondary">
                            <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-layout>
