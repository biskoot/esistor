@push('before-styles')
    <!-- Datatables css -->
    <link href="{{ asset('velonicadmin/vendor/datatables.net-bs5/css/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('velonicadmin/vendor/datatables.net-responsive-bs5/css/responsive.bootstrap5.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('velonicadmin/vendor/datatables.net-fixedcolumns-bs5/css/fixedColumns.bootstrap5.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('velonicadmin/vendor/datatables.net-fixedheader-bs5/css/fixedHeader.bootstrap5.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('velonicadmin/vendor/datatables.net-buttons-bs5/css/buttons.bootstrap5.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('velonicadmin/vendor/datatables.net-select-bs5/css/select.bootstrap5.min.css') }}" rel="stylesheet"
        type="text/css" />
@endpush
@push('before-scripts')
    <!-- Datatables js -->
    <script src="{{ asset('velonicadmin/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-fixedcolumns-bs5/js/fixedColumns.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-buttons-bs5/js/buttons.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-select/js/dataTables.select.min.js') }}"></script>
    <script>
        $('.datatable').DataTable({
            keys: true,
            "language": {
                "paginate": {
                    "previous": "<i class='ri-arrow-left-s-line'>",
                    "next": "<i class='ri-arrow-right-s-line'>"
                }
            },
            "drawCallback": function () {
                $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
            }
        });
    </script>
@endpush

<x-layout :title="'Browse Products'">

    <div class="row">
        <div class="col-12">
            <a href="{{ route('product.create') }}" class="btn btn-primary mb-3">
                <i class="ri-save-line me-1 fs-16 lh-1"></i> New
            </a>
            <div class="card">
                <div class="card-body">
                    <table class="datatable table table-striped dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Product Data Id</th>
                                <th>Storage Type</th>
                                <th>Storage Name</th>
                                <th>Measurement</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products ?? [] as $product)
                            <tr>
                                <td>
                                    {{ $product->id }}
                                </td>
                                <td>
                                    {{ $product->product_data_id }}
                                </td>
                                <td>
                                    {{ $product->storageType->name }}
                                </td>
                                <td>
                                    {{ $product->storage->name }}
                                </td>
                                <td>
                                    {{ $product->measurement_unit->name ?? "" }}
                                </td>
                                <td>
                                    {{ $product->created_at->format('Y-m-d') }}
                                </td>
                                <td>
                                    <a href="{{ route('product.edit',$product) }}" class="btn btn-primary">
                                        <i class="ri-pencil-line me-1 fs-16 lh-1"></i> Edit
                                    </a>
                                    <a href="{{ route('product.show',$product) }}" class="btn btn-secondary">
                                        <i class="ri-search-eye-line me-1 fs-16 lh-1"></i> View
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</x-layout>
