<x-layout :title="'Create Product'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('product.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Product Data</label>
                                <select name="product_data_id" id="product_data_id" class="form-control">
                                    <option value="">--Sila Pilih--</option>
                                    @foreach ($productDatas as $pd)
                                    <option value="{{$pd->id}}">{{$pd->brand.' - '.$pd->item}}</option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Storage Type</label>
                                <select name="storage_type_id" id="storage_type_id" class="form-control">
                                    <option value="">--Sila Pilih--</option>
                                    @foreach ($storageTypes as $st)
                                    <option value="{{$st->id}}">{{$st->name}}</option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Storage</label>
                                <select name="storage_id" id="storage_id" class="form-control">
                                    <option value="">--Sila Pilih--</option>
                                    @foreach ($storages as $st)
                                    <option value="{{$st->id}}">{{$st->name}}</option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Measurement Unit</label>
                                <select name="measurement_unit_id" id="measurement_unit_id" class="form-control">
                                    <option value="">--Sila Pilih--</option>
                                    @foreach ($measurementUnits as $st)
                                    <option value="{{$st->id}}">{{$st->name}}</option>   
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Status</label>
                                <select name="status_code" id="status_code" class="form-control">
                                    <option value="1">AKTIF</option>
                                    <option value="0">ARKIB</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">
                            <i class="ri-add-box-line me-1 fs-16 lh-1"></i> Create
                        </button>
                        <a href="{{ route('crud.index') }}" class="btn btn-secondary">
                            <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-layout>
