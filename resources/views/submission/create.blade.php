<x-layout :title="'Create Dummy CRUD'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('submission.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="product_id">Product Id</label>
                                <input type="text" value="" id="product_id" name="product_id" class="form-control" placeholder="Product">
                            </div>
                        </div>

                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="submission_status_id">Submission Status</label>
                                <input type="text" value="" id="submission_status_id" name="submission_status_id" class="form-control" placeholder="Submission Status">
                            </div>
                        </div>

                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="qty">Quantity</label>
                                <input type="number" value="" id="qty" name="qty" class="form-control" placeholder="Quantity">
                            </div>
                        </div>

                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="requested_by">Requested By</label>
                                <input type="text" value="" id="requested_by" name="requested_by" class="form-control" placeholder="Requested By">
                            </div>
                        </div>

                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="validated_at">Validated At</label>
                                <input type="text" value="" id="validated_at" name="validated_at" class="form-control" placeholder="Validated At">
                            </div>
                        </div>

                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="validated_by">Validated By</label>
                                <input type="text" value="" id="validated_by" name="validated_by" class="form-control" placeholder="Validated By">
                            </div>
                        </div>

                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="status_code">Status Code</label>
                                <input type="text" value="" id="status_code" name="status_code" class="form-control" placeholder="Status Code">
                            </div>
                        </div>

                        <button class="btn btn-primary" type="submit">
                            <i class="ri-add-box-line me-1 fs-16 lh-1"></i> Create
                        </button>
                        <a href="{{ route('submission.index') }}" class="btn btn-secondary">
                            <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-layout>
