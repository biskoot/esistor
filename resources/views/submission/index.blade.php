@push('before-styles')
    <!-- Datatables css -->
    <link href="{{ asset('velonicadmin/vendor/datatables.net-bs5/css/dataTables.bootstrap5.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('velonicadmin/vendor/datatables.net-responsive-bs5/css/responsive.bootstrap5.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('velonicadmin/vendor/datatables.net-fixedcolumns-bs5/css/fixedColumns.bootstrap5.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('velonicadmin/vendor/datatables.net-fixedheader-bs5/css/fixedHeader.bootstrap5.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('velonicadmin/vendor/datatables.net-buttons-bs5/css/buttons.bootstrap5.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('velonicadmin/vendor/datatables.net-select-bs5/css/select.bootstrap5.min.css') }}" rel="stylesheet"
        type="text/css" />
@endpush
@push('before-scripts')
    <!-- Datatables js -->
    <script src="{{ asset('velonicadmin/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-fixedcolumns-bs5/js/fixedColumns.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-buttons-bs5/js/buttons.bootstrap5.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('velonicadmin/vendor/datatables.net-select/js/dataTables.select.min.js') }}"></script>
    <script>
        $('.datatable').DataTable({
            keys: true,
            "language": {
                "paginate": {
                    "previous": "<i class='ri-arrow-left-s-line'>",
                    "next": "<i class='ri-arrow-right-s-line'>"
                }
            },
            "drawCallback": function () {
                $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
            }
        });
    </script>
@endpush

<x-layout :title="'Browse Submission CRUD'">

    <div class="row">
        <div class="col-12">
            <a href="{{ route('submission.create') }}" class="btn btn-primary mb-3">
                <i class="ri-save-line me-1 fs-16 lh-1"></i> New
            </a>
            <div class="card">
                <div class="card-body">
                    <table class="datatable table table-striped dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>Quantity</th>
                                <th>Requested By</th>
                                <th>Created At</th>
                                <th>Validated By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($models ?? [] as $model)
                            <tr>
                                <td>
                                    {{ $model->qty }}
                                </td>
                                <td>
                                    {{ $model->requested_by }}
                                </td>
                                <td>
                                    {{ $model->created_at->format('Y-m-d') }}
                                </td>
                                <td>
                                    {{ $model->validated_by }}
                                </td>
                                <td>
                                    <a href="{{ route('submission.edit', $model) }}" class="btn btn-primary">
                                        <i class="ri-pencil-line me-1 fs-16 lh-1"></i> Edit
                                    </a>
                                    <a href="{{ route('submission.show', $model) }}" class="btn btn-secondary">
                                        <i class="ri-search-eye-line me-1 fs-16 lh-1"></i> View
                                    </a>
                                    <form action="{{ route('submission.destroy', $model)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">
                                            <i class="ri-search-eye-line me-1 fs-16 lh-1"></i> Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</x-layout>
