<x-layout :title="'Display Brand'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('brand.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Brand</label>
                                <input type="text" value="" id="name" name="name" class="form-control" placeholder="Brand name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-3">
                            <label class="form-label" for="FullName">Status</label>
                            <div><select id="status_code" name="status_code" class="form-control">
                                <option value="1">Aktif</option>
                                <option value="0">Arkib</option>
                              </select></div>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">
                            <i class="ri-add-box-line me-1 fs-16 lh-1"></i> Create
                        </button>
                        <a href="{{ route('brand.index') }}" class="btn btn-secondary">
                            <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-layout>
