<x-layout :title="'Brand'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-condensed mb-0 border-top">
                        <tbody>
                            <tr>
                                <th scope="row">Brand</th>
                                <td>
                                    {{$brand->name}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Status</th>
                                <td>
                                    {{$brand->status_code}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="{{ route('brand.index') }}" class="btn btn-secondary mt-2">
                        <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                    </a>
                </div>
            </div>
        </div>
    </div>

</x-layout>
