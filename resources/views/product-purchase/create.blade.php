<x-layout :title="'Create Product Purchase'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('product-purchase.store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="supplier_id" value='3'>
                        <input type="hidden" name="total_price" value='4.2'>
                        <input type="hidden" name="balance" value='44'>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="product_id">Product</label>
                                <select class="form-control" name="product_id" id="product_id">
                                    <option value="">-</option>
                                    <option value="1">Pensil Picit by Syarikat ABC Sdn Bhd</option>
                                    <option value="2">Klip Kertas by Syarikat ABC Sdn Bhd</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="lo_no">LO Number</label>
                                <input type="text" value="" id="lo_no" name="lo_no" class="form-control" placeholder="Enter LO Number">
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="unit_price">Price per unit (RM)</label>
                                <input type="number" id="unit_price" step="0.1" name="unit_price" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="qty">Quantity</label>
                                <input type="number" id="qty" name="qty" class="form-control">
                            </div>
                        </div>

                        <button class="btn btn-primary" type="submit">
                            <i class="ri-add-box-line me-1 fs-16 lh-1"></i> Create
                        </button>
                        <a href="{{ route('product-purchase.index') }}" class="btn btn-secondary">
                            <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-layout>
