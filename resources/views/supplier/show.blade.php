<x-layout :title="'Show Dummy CRUD'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-condensed mb-0 border-top">
                        <tbody>
                            <tr>
                                <th scope="row">ID</th>
                                <td>
                                    {{ $models->id }}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Nama Pembekal</th>
                                <td>
                                    {{ $models->name }}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Kod Status</th>
                                <td>
                                    {{ $models->status_code }}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Created At</th>
                                <td>
                                    {{ $models->created_at }}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Created By</th>
                                <td>
                                    {{ $models->created_by }}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Updated At</th>
                                <td>
                                    {{ $models->updated_at }}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Updated By</th>
                                <td>
                                    {{ $models->updated_by }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="{{ route('supplier.index') }}" class="btn btn-secondary mt-2">
                        <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                    </a>
                </div>
            </div>
        </div>
    </div>

</x-layout>
