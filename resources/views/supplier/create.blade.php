<x-layout :title="'Tambah Nama Pembekal'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('supplier.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Pembekal</label>
                                <input type="text" value="" id="name" name="name" class="form-control" placeholder="Nama Pembekal">
                            </div>
                        </div>

                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="StatusCode">Kod Pembekal</label>
                                <input type="number" value="" id="status_code" name="status_code" class="form-control" placeholder="Kod Pembekal">
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">
                            <i class="ri-add-box-line me-1 fs-16 lh-1"></i> Tambah
                        </button>
                        <a href="{{ route('supplier.index') }}" class="btn btn-secondary">
                            <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Kembali
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-layout>
