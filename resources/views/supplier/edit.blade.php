<x-layout :title="'Edit Dummy CRUD'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('supplier.update', $supplier) }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Pembekal</label>
                                <input type="text" id="name" name="name" class="form-control" placeholder="Nama Pembekal" value="{{$supplier->name}}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="StatusCode">Kod Pembekal</label>
                                <input type="number" id="status_code" name="status_code" class="form-control" placeholder="Kod Pembekal" value="{{$supplier->status_code}}">
                            </div>
                        </div>

                        <button class="btn btn-primary" type="submit">
                            <i class="ri-save-line me-1 fs-16 lh-1"></i> Save
                        </button>
                        <a href="{{ route('supplier.index') }}" class="btn btn-secondary">
                            <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-layout>
