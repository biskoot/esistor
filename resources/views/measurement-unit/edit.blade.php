<x-layout :title="'Edit Dummy CRUD'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('measurement-unit.update', $MeasurementUnit) }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Name</label>
                                <input type="text" value="{{ $MeasurementUnit->name}}" id="name" name="name" class="form-control" placeholder="Input Placeholder">
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Status Code</label>
                                <input type="number" value="{{ $MeasurementUnit->name}}" id="name" name="status_code" class="form-control" placeholder="Input Placeholder">
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">
                            <i class="ri-save-line me-1 fs-16 lh-1"></i> Save
                        </button>
                        <a href="{{ route('crud.index') }}" class="btn btn-secondary">
                            <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-layout>
