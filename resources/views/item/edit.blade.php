<x-layout :title="'Edit Dummy CRUD'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('item.update',$item) }}" method="POST">
                        @method('PATCH')
                        @csrf
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Item Name :</label>
                                <input type="text" value="{{ $item->name }}" id="name" name="name" class="form-control" >
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Status Code :</label>
                                <input type="number"value="{{ $item->status_code }}" id="name" name="status_code" class="form-control">
                            </div>
                            
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Created By :</label>
                                <input type="date" value="{{ $item->created_by }}" id="name" name="created_by" class="form-control" >
                            </div>

                            <div class="mb-2">
                                <label class="form-label" for="FullName">Updated By :</label>
                                <input type="date" value="{{ $item->updated_by }}" id="name" name="updated_by" class="form-control" >
                            </div>  
                        </div>
                        <button class="btn btn-primary" type="submit">
                            <i class="ri-save-line me-1 fs-16 lh-1"></i> Save
                        </button>
                        <a href="{{ route('item.index') }}" class="btn btn-secondary">
                            <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-layout>
