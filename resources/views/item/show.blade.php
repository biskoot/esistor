<x-layout :title="'Show Product Purchase'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-condensed mb-0 border-top">
                        <tbody>
                            <tr>
                                <th scope="row">ID</th>
                                <td>
                                    {{$product_purchase->id}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Product</th>
                                <td>
                                    {{$product_purchase->product_id}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Supplier</th>
                                <td>
                                    {{$product_purchase->supplier_id}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Quantity</th>
                                <td>
                                    {{$product_purchase->qty}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Price per unit (RM)</th>
                                <td>
                                    {{$product_purchase->unit_price}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">LO Number</th>
                                <td>
                                    {{$product_purchase->lo_no}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Date Purchased</th>
                                <td>
                                    {{$product_purchase->created_at}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="{{ route('item.index') }}" class="btn btn-secondary mt-2">
                        <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                    </a>
                </div>
            </div>
        </div>
    </div>

</x-layout>
