<x-layout :title="'Create Item CRUD'">

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('item.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Item Name :</label>
                                <input type="text" value="" id="name" name="name" class="form-control" >
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Status Code :</label>
                                <input type="number" value="" id="name" name="status_code" class="form-control">
                            </div>
                            
                            <div class="mb-2">
                                <label class="form-label" for="FullName">Created By :</label>
                                <input type="date" value="" id="name" name="created_by" class="form-control" >
                            </div>

                            <div class="mb-2">
                                <label class="form-label" for="FullName">Updated By :</label>
                                <input type="date" value="" id="name" name="updated_by" class="form-control" >
                            </div>  
                        </div>
                        <button class="btn btn-primary" type="submit">
                            <i class="ri-add-box-line me-1 fs-16 lh-1"></i> Create
                        </button>
                        <a href="{{ route('item.index') }}" class="btn btn-secondary">
                            <i class="ri-arrow-go-back-line me-1 fs-16 lh-1"></i> Back
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-layout>
