<!-- ========== Left Sidebar Start ========== -->
<div class="leftside-menu">

    <!-- Brand Logo Light -->
    <a href="index.html" class="logo logo-light">
        <span class="logo-lg">
            <img src="{{ asset('velonicadmin/images/logo.png') }}" alt="logo">
        </span>
        <span class="logo-sm">
            <img src="{{ asset('velonicadmin/images/logo-sm.png') }}" alt="small logo">
        </span>
    </a>

    <!-- Brand Logo Dark -->
    <a href="index.html" class="logo logo-dark">
        <span class="logo-lg">
            <img src="{{ asset('velonicadmin/images/logo-dark.png') }}" alt="dark logo">
        </span>
        <span class="logo-sm">
            <img src="{{ asset('velonicadmin/images/logo-sm.png') }}" alt="small logo">
        </span>
    </a>

    <!-- Sidebar -left -->
    <div class="h-100" id="leftside-menu-container" data-simplebar>
        <!--- Sidemenu -->
        <ul class="side-nav">

            <li class="side-nav-title">Main</li>

            <li class="side-nav-item">
                <a href="template/index.html" class="side-nav-link">
                    <i class="ri-profile-line"></i>
                    <span> Template </span>
                </a>
            </li>

            <li class="side-nav-item">
                <a href="{{ route('dashboard') }}" class="side-nav-link">
                    <i class="ri-dashboard-3-line"></i>
                    <span> Dashboard </span>
                </a>
            </li>

            <li class="side-nav-item">
                <a href="#" class="side-nav-link">
                    <i class="ri-edit-2-line"></i>
                    <span> DUMMY CRUD </span>
                </a>
            </li>
            <li class="side-nav-item">
                <a href="{{ route('storage.index') }}" class="side-nav-link">
                    <i class="ri-edit-2-line"></i>
                    <span>Storage </span>
                </a>
            </li>

            <li class="side-nav-item">
                <a href="{{ route('submission.index') }}" class="side-nav-link">
                    <i class="ri-edit-2-line"></i>
                    <span> Submission </span>
                </a>
            </li>

            <li class="side-nav-item">
                <a href="{{ route('category.index') }}" class="side-nav-link">
                    <i class="ri-edit-2-line"></i>
                    <span> Category </span>
                </a>
            </li>

            <li class="side-nav-item">
                <a href="{{ route('product-purchase.index') }}" class="side-nav-link">
                    <i class="ri-archive-line"></i>
                    <span>Product Purchase</span>
                </a>
            </li>

            <li class="side-nav-item">
                <a href="{{ route('supplier.index') }}" class="side-nav-link">
                    <i class="ri-edit-2-line"></i>
                    <span> Supplier </span>
                </a>
            </li>
            <li class="side-nav-item">
                <a href="{{ route('brand.index') }}" class="side-nav-link">
                    <i class="ri-edit-2-line"></i>
                    <span> Brand </span>
                </a>
            </li>

            <li class="side-nav-item">
                <a href="{{ route('measurement-unit.index') }}" class="side-nav-link">
                    <i class="ri-edit-2-line"></i>
                    <span> Measurement Unit </span>
                </a>
            </li>
            <li class="side-nav-item">
                <a href="{{ route('product-data.index') }}" class="side-nav-link">
                    <i class="ri-edit-2-line"></i>
                    <span> Product Data</span>
                </a>
            </li>
            <li class="side-nav-item">
                <a href="{{ route('product.index') }}" class="side-nav-link">
                    <i class="ri-edit-2-line"></i>
                    <span> Product </span>
                </a>
            </li>
            <!-- add other sidebar link here -->
            <li class="side-nav-item">
                <a href="{{ route('item.index') }}" class="side-nav-link">
                    <i class="ri-edit-2-line"></i>
                    <span> Item </span>
                </a>
            </li>

        </ul>
        <!--- End Sidemenu -->

        <div class="clearfix"></div>
    </div>
</div>
<!-- ========== Left Sidebar End ========== -->
